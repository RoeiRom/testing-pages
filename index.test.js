const { add } = require('./index');

describe('add tests:', () => {
    it('should add the given numbers', () => {
        expect(add(1, 2)).toEqual(3);
    })
})